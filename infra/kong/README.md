Kong Local configuration

### Kong.conf

Create the docker network
```
docker network create kong-net
```    
    
Create the postgres instance
```
docker run -d --name kong-database \
               --network=kong-net \
               -p 5432:5432 \
               -e "POSTGRES_USER=kong" \
               -e "POSTGRES_DB=kong" \
               postgres:9.6
```

Build Kong local image with the specific configuration.  
We had the specific `kong.conf` configuration file into `/etc/kong.conf` file.
```
build -t kong-local .
```
Because, the Kong server cannot be expose over https on your local environment, we configure the server to  
accepts HTTPs requests that have already been terminated by a proxy or load balancer  
and the `x-forwarded-proto: https` header has been added to the request.  

Start Kong server with `kong-local` image

```
   docker run -d --name kong \
     --network=kong-net \
     -e "KONG_DATABASE=postgres" \
     -e "KONG_PG_HOST=kong-database" \
     -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
     -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
     -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_LISTEN=0.0.0.0:8001" \
     -p 8000:8000 \
     -p 8001:8001 \
     kong-local:latest
```

### Api-device and api-pets configuration

#### Routing
```
curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=api-devices’ \
  --data 'url=http://api-devices:8080'

curl -i -X POST \
  --url http://localhost:8001/services/api-devices/routes \
  --data 'hosts[]=api-devices'

curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=api-pets’ \
  --data 'url=http://api-pets:8080'
    
curl -i -X POST \
  --url http://localhost:8001/services/api-pets/routes \
  --data 'hosts[]=api-pets'
```

Check your apis configuration are working
```
curl -i -X GET http://localhost:8000/healthcheck --header 'Host: api-devices'
curl -i -X GET http://localhost:8000/v2/pets?status=available --header 'Host: api-pets'
```

#### Protect api-pets with api-key plugins

https://docs.konghq.com/hub/kong-inc/key-auth/

```
curl -X POST http://kong:8001/services/api-pets/plugins \
    --data "name=key-auth"
    
curl -d "username=daniel&custom_id=daniel.sabin" http://kong:8001/consumers/    
curl -X POST http://kong:8001/consumers/daniel.sabin/key-auth -d ''
```

Check your api-pets configuration is working
```
curl -i -X GET http://localhost:8000/v2/pets?status=available --header 'Host: api-pets' --header 'apikey: <some_key>'
```

#### Protect api-pets with oauth2 plugin
Configure the plugin
```
curl -X POST http://localhost:8001/services/api-pets/plugins \
        --data 'name=oauth2' \
        --data 'config.scopes=write:pets,read:pets' \
        --data 'config.accept_http_if_already_terminated=true' \
        --data 'config.mandatory_scope=true' \
        --data 'config.enable_implicit_grant=true'
```
Create the consumer
```
curl -X POST http://localhost:8001/consumers/ \
        --data 'username=developer.ocac' \
        --data 'custom_id=a08c905d-63ea-4086-8c5c-ab0f4f891045'
```
Create the application
```
curl -X POST http://localhost:8001/consumers/<consumer_cutsom_id>/oauth2 \
        --data 'name=mon-application-front' \
        --data 'redirect_uris=http://localhost:4200/postLogin'
```
Check configuration with
```
http://localhost:8001/oauth2?client_id=<your-client-id>
http://localhost:8001/consumers/<consumer_cutsom_id>/oauth2
```
Ask for a token with `curl`
```
curl -v -X POST http://localhost:8000/oauth2/authorize \
    --header 'Host: api-pets' \
    --header 'x-forwarded-proto: https' \
    --data 'client_id=<your-client-id>'\
    --data 'response_type=token'\
    --data 'scope=read:pets' \
    --data 'provision_key=<your-provision-key>'\
    --data 'authenticated_userid=<consumer_custom_id>'
```
Read tokens
```
http://localhost:8001/oauth2_tokens
```
Run the oauth2 login form
```
cd oauth2-login
docker build -t oauth2-kong-login .
docker run -d --name oauth2-kong-login -p 3000:3000 \
    --network kong-net \
    -e "PROVISION_KEY=Yq3xNaEbGkiwoQ0Yhi6AYYM2Pgy8vONi" \
    -e "SERVICE_HOST=ocac-apism-api1" \
    -e "KONG_ADMIN=http://kong:8001" \
    -e "KONG_API=http://kong:8000" \
    -e "API_PATH=/" \
    -e "SCOPES=\"{read:healthcheck}\"" \
    oauth2-kong-login:latest
```
Ask for a token with `oauth2-login-form`
```
http://localhost:3000/authorize?client_id=<your-client-id>&response_type=token&scope=read%3Apets
```

