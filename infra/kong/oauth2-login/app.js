const request = require('request')
const url = require('url')
const bodyParser = require('body-parser')
const express = require("express")
const uuid = require('uuid/v4')
const querystringLib = require('querystring');
const session = require('express-session')
const FileStore = require('session-file-store')(session)
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

const users = [
  {id: 'a08c905d-63ea-4086-8c5c-ab0f4f891045', email: 'test@test.com', password: 'password'}
]

passport.use(new LocalStrategy(
    { usernameField: 'email' },
    (email, password, done) => {
      const user = users[0]
      if(email === user.email && password === user.password) {
        return done(null, user)
      }
      return done(null, null)
    }
))
passport.serializeUser((user, done) => { done(null, user.id) })
passport.deserializeUser((id, done) => {
  const user = users[0].id === id ? users[0] : false
  done(null, user)
})

const app = express()
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
  genid: (req) => {
    return uuid()
  },
  store: new FileStore(),
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function load_env_variable(name) {
  let value = process.env[name];
  if (value) {
    return value
  } else {
    console.error("You need to specify a value for the environment variable: " + name);
    process.exit(1)
  }
}

const PROVISION_KEY = load_env_variable("PROVISION_KEY")
const SERVICE_HOST = load_env_variable("SERVICE_HOST")
const KONG_ADMIN = load_env_variable("KONG_ADMIN")
const KONG_API = load_env_variable("KONG_API")
const API_PATH = load_env_variable("API_PATH")
const SCOPE_DESCRIPTIONS =JSON.parse(load_env_variable("SCOPES") || {})
const LISTEN_PORT = process.env["LISTEN_PORT"] || 3000

function get_application_name(client_id, callback) {
  request({
    method: "GET",
    url: KONG_ADMIN + "/oauth2",
    qs: { client_id: client_id }
  }, function(error, response, body) {
    let application_name;
    if (client_id && !error) {
      let json_response = JSON.parse(body)
      if (json_response.data.length == 1) {
        application_name = json_response.data[0].name
      }
    }
    callback(application_name)
  })
}

function authorize(client_id, response_type, scope, user, callback) {
  console.log(KONG_API + API_PATH + "oauth2/authorize")
  console.log(client_id)
  console.log(response_type)
  console.log(scope)
console.log(user.id)
  request({
    method: "POST",
    url: KONG_API + API_PATH + "oauth2/authorize",
    headers: {
      Host: SERVICE_HOST,
      'X-Forwarded-Proto': 'https'
    },
    form: {
      client_id: client_id, 
      response_type: response_type, 
      scope: scope, 
      provision_key: PROVISION_KEY,
      authenticated_userid: user.id
    }
  }, function(error, response, body) {

    console.log('error',error)
    console.log('response', response)
    console.log('body', body)

    callback(JSON.parse(body).redirect_uri);
  });
}

app.get('/authorize', function(req, res) {
  let querystring = url.parse(req.url, true).query
  let queryParams = querystringLib.stringify(url.parse(req.url, true).query)
  if(!req.isAuthenticated()) {
    res.redirect('/login' + '?' + queryParams)
    return
  }

  get_application_name(querystring.client_id, function(application_name) {
    application_name = 'fake'
    if (application_name) {
      res.render('authorization', {
        user_email: req.user.email,
        client_id: querystring.client_id,
        response_type: querystring.response_type,
        scope: querystring.scope,
        application_name: application_name,
        SCOPE_DESCRIPTIONS: SCOPE_DESCRIPTIONS 
      });
    } else {
      res.status(403).send("Invalid client_id");
    }
  })
})

app.post('/authorize', function(req, res) {
  authorize(req.body.client_id, req.body.response_type, req.body.scope, req.user, function(redirect_uri) {
    res.redirect(redirect_uri);
  });
});

app.get('/login', (req, res) => {
  let queryParams = querystringLib.stringify(url.parse(req.url, true).query)
  let querystring = url.parse(req.url, true).query
  if(req.isAuthenticated()) {
    res.redirect('/authorize' + '?' + queryParams)
  }else {
    res.render('login',{
      client_id: querystring.client_id,
      response_type: querystring.response_type,
      scope: querystring.scope
    })
  }
})

app.post('/login', (req, res, next) => {
  let queryParams = querystringLib.stringify(req.body)

  passport.authenticate('local', (err, user, info) => {
    if(info) {return res.send(info.message)}
    if (err) { return next(err); }
    if (!user) {
      if (req.body.password) {
        delete req.body.password
      }
      let queryParams = querystringLib.stringify(req.body)
      return res.redirect('/login' + '?' + queryParams)
    }
    req.login(user, (err) => {
      if (err) { return next(err); }
      if (req.body.password) {
        delete req.body.password
      }
      let queryParams = querystringLib.stringify(req.body)
      return res.redirect('/authorize' + '?' + queryParams)
    })
  })(req, res, next);
})

app.listen(LISTEN_PORT);

console.log("Running at Port " + LISTEN_PORT);
